import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import AppLoader from './Components/Loader/Loader';
import Navbar from './Components/Navbar/Navbar';
import Login from './Pages/Login/Login';
const Details = React.lazy(() => import("./Pages/Details/Details"));
const OnBoard = React.lazy(() => import("./Pages/OnBoard/OnBoard"));
const Users = React.lazy(() => import("./Pages/Users/Users"));
const OffBoard = React.lazy(() => import("./Pages/OffBoard/OffBoard"));

function App() {

  const [token, setToken] = useState(true);
  const login = (email, password) => {
    if(email === 'shubham.joshi2812@gmail.com' && password === 'Shubh66!') {
      window.localStorage.setItem('authToken', 'eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6ImFkbWluQGFkbWluLmNvbSIsImV4cCI6MTYzOTc0MjgxMywiaWF0IjoxNjM3MTUwODEzfQ.LLjVPkGWgXv-jfRMl3LzeggOvE3UBsc_pUMRFEyhiWo');
      setToken(true);
      return true;
    }
    else {
      setToken(false);
      return false;
    }
  }

  if(!window.localStorage.getItem('authToken')) {
    return (<Login setToken={setToken} token={token} handleLogin={login}/>)
  }
  return (
    <div className="App">
      <React.Suspense fallback={<AppLoader/>}>
      <Router>
      <Navbar/>
      <Routes>
      <Route path='/users' exact element={<Users />} />
    <Route path='/on-board' element={<OnBoard />} />
          
    <Route path='/off-board' element={<OffBoard />} />
    <Route path='/users/:id' element={<Details/>}/>
      </Routes>
      </Router>
      </React.Suspense>
    </div>
  );
}

export default App;
