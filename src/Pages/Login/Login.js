import React, {useState} from 'react'
import { Formik } from 'formik';
import './Login.css'


function Login({handleLogin}){
    const [message, setMessage] = useState(null)
    let isLoggedIn = true;
    return (
        <div className="on-board">
            <div className="form-container">
            <Formik
       initialValues={{ email: '', password: ''}}
       validate={values => {
         const errors = {};
         if (!values.email) {
           errors.email = 'Email is required';
         } else if (
           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
         ) {
           errors.email = 'Invalid email address';
         }

        if(!values.password) {
            errors.password = 'Password is required'
        } else if(!/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i.test(values.password)) {
            errors.password = 'Invalid password'
        }
         return errors;
       }}
       onSubmit={async (values, { setSubmitting }) => {
        isLoggedIn = handleLogin(values.email, values.password);
        console.log(isLoggedIn);
        if(isLoggedIn) {
            window.location.href = '/users'
        } else {
            setMessage('Invalid Credentials')
        }
        setSubmitting(false);
       }}
     >{({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
                <form className="register-form" onSubmit={handleSubmit}>
                    <span className="error-msg">{message ? message : null}</span>
                    <input id="email" type="text" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Email" value={values.email}/>
                    <span className='error-msg'>{errors.email && touched.email && errors.email}</span>
                    <input id="password" type="password" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Password" value={values.password}/>
                    <span className='error-msg'>{errors.password && touched.password && errors.password}</span>
                    <button className="form-field" onClick={() => console.log(values)} disabled={isSubmitting} type="submit">Login</button>
                </form>)}
            </Formik>
            </div>
        </div>
    )
}

export default Login;
