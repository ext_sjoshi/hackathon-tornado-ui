import React from 'react'
import * as FaIcon from "react-icons/fa";
import { Formik } from 'formik';
import {callAPI} from "../../utlis";
import './OnBoard.css'
import {useNavigate} from "react-router-dom";

function OnBoard() {
    const navigate = useNavigate();
    return (
        <div className="on-board">
            <div className="form-container">
            <Formik
       initialValues={{ firstName:'', lastName: '', email: '', password: '', slack: false, jira: false }}
       validate={values => {
         const errors = {};
         if (!values.email) {
           errors.email = 'Email is required';
         } else if (
           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
         ) {
           errors.email = 'Invalid email address';
         }

         if(!values.firstName) {
             errors.firstName = 'First name is required'
         } else if(!/^[a-z ,.'-]+$/i.test(values.firstName)) {
            errors.firstName = 'Invalid first name';
         }

         if(!values.lastName) {
            errors.lastName = 'Last name is required'
        } else if(!/^[a-z ,.'-]+$/i.test(values.firstName)) {
           errors.lastName = 'Invalid last name';
        }

        if(!values.password) {
            errors.password = 'Password is required'
        } else if(!/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i.test(values.password)) {
            errors.password = 'Invalid password'
        }
         return errors;
       }}
       onSubmit={async (values, { setSubmitting, resetForm }) => {
        await callAPI('POST', '/user', JSON.stringify(values, null, 2));
        setSubmitting(false);
        resetForm();
        navigate('/users')
       }}
     >{({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
                <form className="register-form" onSubmit={handleSubmit}>
                    <input id="firstName" type="text" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="First Name" value={values.firstName}/>
                    <span className='error-msg'>{errors.firstName && touched.firstName && errors.firstName}</span>
                    <input id="lastName" type="text" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Last Name" value={values.lastName}/>
                    <span className='error-msg'>{errors.lastName && touched.lastName && errors.lastName}</span>
                    <input id="email" type="text" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Email" value={values.email}/>
                    <span className='error-msg'>{errors.email && touched.email && errors.email}</span>
                    <input id="password" type="password" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Password" value={values.password}/>
                    <span className='error-msg'>{errors.password && touched.password && errors.password}</span>
                    <div className="action-input">
                        <div>
                            <input name="slack" className="form-field" type="checkbox" onChange={handleChange} checked={values.slack}/>&nbsp;&nbsp;
                            <label><FaIcon.FaSlack/>  Assign Slack</label>
                        </div>
                    </div>
                    <div className="action-input">
                        <div>
                            <input name="jira" className="form-field" type="checkbox" onChange={handleChange} checked={values.jira}/>&nbsp;&nbsp;
                            <label><FaIcon.FaJira/>  Assign Jira</label>
                        </div>
                    </div>
                    <button className="form-field" onClick={() => console.log(values)} disabled={isSubmitting} type="submit">Create User</button>
                </form>)}
            </Formik>
            </div>
        </div>
    )
}

export default OnBoard
