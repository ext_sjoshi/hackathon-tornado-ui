import React, {useEffect, useState} from 'react'
import { headers} from "./UsersData";
import {Link} from "react-router-dom";
import "./Users.css"
import {useNavigate} from "react-router-dom";
import {callAPI} from "../../utlis";

function Users() {
    const navigate = useNavigate();
    const [users, setUsers] = useState([])
    useEffect(() => {

        const getData = async() => {
            const response = await callAPI('GET', 'list-users');
            setUsers(response.data);
        }
        getData();
    }, [])
    return (
        <div className="users">
            <div className="list">
                <div className="row-dark">
                {
                    headers.map(heading => <span>{heading}</span>)
                }
                </div>
                {
                    !users || !users.length ? (<div className="empty-state-wrapper">
                        <div className="empty-state">
                        No Data Found
                        <button className="form-field" onClick={() => navigate('/on-board')}>Create User</button>
                        </div>    
                    </div>) :
                     users.map(({id, userName, emailId, azureAdId, slackId, jiraId}, index) => {
                        return(<div className={index % 2 ? 'row-dark' : 'row-light'}><span>{id}</span> <Link onClick={() => window.localStorage.setItem('data', JSON.stringify({jiraId, slackId}))} className="link" to={`/users/${azureAdId}`}> {userName}</Link><span>{emailId}</span></div>)
                    })
                }
            </div>
        </div>
    )
}

export default Users;