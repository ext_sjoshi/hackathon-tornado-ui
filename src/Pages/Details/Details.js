import React, {useState, useEffect} from 'react'
import * as FaIcon from "react-icons/fa";
import { useParams } from 'react-router-dom';
import {callAPI} from '../../utlis';
import "./Details.css"
import {useNavigate} from "react-router-dom";

function Details() {
    const navigate = useNavigate();
    let { id } = useParams();
    const data = JSON.parse(window.localStorage.getItem('data'));
    console.log(data);
    const [user, setUser] = useState({})
    useEffect(() => {
        const getData = async() => {
            const response = await callAPI('GET', `/user?id=${id}`);
            setUser(response.data);
        }
        getData();
    }, [])

    const assignJira = async () => {
        try{
            await callAPI('GET', `send-invite?userMail=${user.userPrincipalName}`)
            navigate('/users')
        } catch(error) {
            console.log(error);
        }
    }
    const handleChange = async (id) => {
        console.log('iiiiiid',typeof id);
        if(!id || !id.length) {
            assignJira()
            return;
        }
        try {
            await callAPI('DELETE', `/revoke-jira-access?jiraId=${id}`)
            navigate('/users')
        } catch(error) {
            console.log(error);
        }
    }
    return (
        <div className="details">
            <h3>Azure AD Details</h3>
            <div className="list">
                <div className="head">{['fields', 'data'].map(key => <span className="cell">{key}</span>)}</div>
                {!user || !Object.keys(user) ? (<div className="empty-state">No data found for this user</div>) :
                <div>{Object.keys(user).map(key => (<div className="head"><span className="cell">{key}</span><span className="cell">{user[key]}</span></div>))}</div>}
            </div>

            {!user || !Object.keys(user) ? null : <div className="actions">
            <div className="action-input">
                        <div>
                            <input name="slack" id={data.slackId} className="form-field" type="checkbox" onChange={(e) => handleChange(e.target.id)} checked={data.slackId}/>&nbsp;&nbsp;
                            <label><FaIcon.FaSlack/>  Assign Slack</label>
                        </div>
                    </div>
                    <div className="action-input">
                        <div>
                            <input name="jira" id={data.jiraId} className="form-field" type="checkbox" onChange={(e) => handleChange(e.target.id)} checked={data.jiraId}/>&nbsp;&nbsp;
                            <label><FaIcon.FaJira/>  Assign Jira</label>
                        </div>
                    </div>
            </div>}
        </div>
    )
}

export default Details
