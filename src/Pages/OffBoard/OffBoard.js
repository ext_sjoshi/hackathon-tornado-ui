import React from 'react'
import { Formik } from 'formik';
import {callAPI} from "../../utlis"

function OffBoard() {
    return (
        <div className="offBoard">
            <div className="form-container">
            <Formik
       initialValues={{ email: ''}}
       validate={values => {
         const errors = {};
         if (!values.email) {
           errors.email = 'Email is required';
         } else if (
           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
         ) {
           errors.email = 'Invalid email address';
         }

         return errors;
       }}
       onSubmit={async (values, { setSubmitting, resetForm }) => {
        await callAPI('DELETE', `/user?id=${values.email}`, JSON.stringify(values, null, 2));
        setSubmitting(false);
        resetForm();
       }}
     >{({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
                <form className="register-form" onSubmit={handleSubmit}>
                    <input id="email" type="text" onChange={handleChange} onBlur={handleBlur} className="form-field" placeholder="Email" value={values.email}/>
                    <span className='error-msg'>{errors.email && touched.email && errors.email}</span>
                    <button className="form-field" onClick={() => console.log(values)} disabled={isSubmitting} type="submit">Off Board</button>
                </form>
      )}
      </Formik>
        </div>
        </div>
    )
}

export default OffBoard
