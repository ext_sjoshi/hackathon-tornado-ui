import * as FaIcon from "react-icons/fa";

export const navItems = [{
    title: 'User',
    path: '/users',
    icon: <FaIcon.FaUsers/>,
    cName: 'nav-text'
},
{
    title: 'On Board',
    path: '/on-board',
    icon: <FaIcon.FaUserPlus/>,
    cName: 'nav-text'
},
{
    title: 'Off Board',
    path: '/off-board',
    icon: <FaIcon.FaUserMinus/>,
    cName: 'nav-text'
}
]