import React, {useState} from 'react'
import {Link} from "react-router-dom";
import * as FaIcons from "react-icons/fa";
import * as AiIcon from "react-icons/ai";
import * as IoIcon from "react-icons/io";
import {navItems} from "./NavbarData";
import './Navbar.css';
import {IconContext} from 'react-icons';

function Navbar() {
    const [sidebar, setSideBar] = useState(false);

    const toggleSidebar = () => setSideBar(!sidebar);

    return (
        <>
        <IconContext.Provider value={{color: '#fff'}}>
          <div className="navbar">
              <Link to="#" className="menu-bars">
                  <FaIcons.FaBars onClick={toggleSidebar}/>
              </Link>
              <Link to="#" className="menu-bars">
                  <IoIcon.IoIosExit onClick={() => {  window.localStorage.removeItem('authToken'); window.location.href = '/'}}/>
              </Link>
            </div>  
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                <ul className="nav-menu-items">
                    <li className="navbar-toggle" onClick={toggleSidebar}>
                        <Link to="#" className="menu-bars">
                            <AiIcon.AiOutlineClose/>
                        </Link>
                    </li>
                    {
                        navItems.map((item, index) => (<li key={index} className={item.cName}><Link to={item.path}>{item.icon} <span>{item.title}</span></Link></li>))
                    }
                </ul>
            </nav>
            </IconContext.Provider>
        </>
    )
}

export default Navbar
