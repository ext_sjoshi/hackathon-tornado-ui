import axios from "axios";

export async function callAPI(method, url, data) {
  try {
    const response = await axios({
      baseURL: 'http://2eda-202-168-84-87.ngrok.io/hackathon',
      method: method,
      url, 
      data: data, 
      headers:{'Content-Type': 'application/json; charset=utf-8'}
  })    
    return {success: true, data: response.data}
  } catch(error) {
    return {success: false, error}
  }
}
